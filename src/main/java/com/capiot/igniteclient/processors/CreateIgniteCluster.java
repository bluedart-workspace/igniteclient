package com.capiot.igniteclient.processors;

import org.apache.camel.Exchange;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.IgniteClientDisconnectedException;
import org.apache.ignite.Ignition;
import org.apache.ignite.configuration.CacheConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.spi.communication.tcp.TcpCommunicationSpi;
import org.apache.ignite.spi.discovery.tcp.TcpDiscoverySpi;
import org.apache.ignite.spi.discovery.tcp.ipfinder.vm.TcpDiscoveryVmIpFinder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import javax.cache.Cache.Entry;
import javax.cache.CacheException;
import javax.cache.expiry.Duration;
import javax.cache.expiry.ModifiedExpiryPolicy;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;


public class CreateIgniteCluster {


    private static final Logger log = LoggerFactory.getLogger(CreateIgniteCluster.class);

    public CreateIgniteCluster() {
    }

    private static IgniteCache<String, String> cache;
    CacheConfiguration cachecfg = new CacheConfiguration();

    private static Ignite ignite;

    @Value("${ipList}")
    private String iplist;

//    public String getIplist() {
//        return this.iplist;
//    }
//
//    public void setIplist(String iplist) {
//        this.iplist = iplist;
//    }


    public void createCluster(Exchange exchange) throws Exception {
        String[] clusterIplist = this.iplist.split(",");
        TcpDiscoverySpi spi = new TcpDiscoverySpi();
        TcpDiscoveryVmIpFinder ipFinder = new TcpDiscoveryVmIpFinder();
        ipFinder.setAddresses(Arrays.asList(clusterIplist));
        spi.setIpFinder(ipFinder);
        spi.setNetworkTimeout(120000L);
        TcpCommunicationSpi commSpi = new TcpCommunicationSpi();
        commSpi.setSlowClientQueueLimit(1000);
        IgniteConfiguration cfg = new IgniteConfiguration();
        cfg.setDiscoverySpi(spi);
        cfg.setMetricsLogFrequency(0L);
        cfg.setIgniteInstanceName("IgniteClient");
        cfg.setClientMode(true);
        ignite = Ignition.start(cfg);
    }

    public void putData(String key, String Value, String cacheName, Integer timeToLive) {
        this.cachecfg.setName(cacheName);
        if (timeToLive != 0) {
            Duration duration = new Duration(TimeUnit.SECONDS, timeToLive);
            this.cachecfg.setExpiryPolicyFactory(ModifiedExpiryPolicy.factoryOf(duration));
        }
        try {
            cache = ignite.getOrCreateCache(this.cachecfg);
            cache.put(key, Value);
        } catch (CacheException var7) {
            if (var7.getCause() instanceof IgniteClientDisconnectedException) {
                IgniteClientDisconnectedException cause = (IgniteClientDisconnectedException) var7.getCause();
                cause.reconnectFuture().get();
            }
        }
    }

    public String getData(String key, String cacheName) {
        this.cachecfg.setName(cacheName);
        String value = new String();

        try {
            cache = ignite.getOrCreateCache(this.cachecfg);
            value = cache.get(key);
        } catch (CacheException var6) {
            if (var6.getCause() instanceof IgniteClientDisconnectedException) {
                IgniteClientDisconnectedException cause = (IgniteClientDisconnectedException) var6.getCause();
                cause.reconnectFuture().get();
            }
        }
        return value;
    }

    public HashMap<String, String> getAllData(String cacheName) {
        this.cachecfg.setName(cacheName);
        HashMap<String, String> cacheMap = new HashMap<>();

        try {
            cache = ignite.getOrCreateCache(this.cachecfg);
//            Iterator<Cache.Entry<String, String>> iter = cache.iterator();
            Iterator var7 = cache.iterator();
            while (var7.hasNext()) {
                Entry<String, String> stringEntry = (Entry<String, String>) var7.next();
                cacheMap.put(stringEntry.getKey(), stringEntry.getValue());
            }
        } catch (CacheException var6) {
            if (var6.getCause() instanceof IgniteClientDisconnectedException) {
                IgniteClientDisconnectedException cause = (IgniteClientDisconnectedException) var6.getCause();
                cause.reconnectFuture().get();
            }
        }
        return cacheMap;
    }

    @Override
    public String toString() {
        return "CreateIgniteCluster{" +
                "cachecfg=" + cachecfg +
                ", iplist='" + iplist + '\'' +
                '}';
    }
}
