package com.capiot.igniteclient.processors;

import com.capiot.igniteclient.resources.IgniteCacheResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class AccessCache {


    private static final Logger log = LoggerFactory.getLogger(AccessCache.class);

    public AccessCache() {
    }

    public IgniteCacheResource process(IgniteCacheResource igniteCacheResource) throws Exception {

        IgniteCacheResource cacheResource = new IgniteCacheResource();

        Integer timeToLive = 0;
        CreateIgniteCluster createIgniteCluster = new CreateIgniteCluster();
        String key = igniteCacheResource.getCacheKey();
        String cacheName = igniteCacheResource.getCacheName();
        String value = igniteCacheResource.getCacheValue();
        String operation = igniteCacheResource.getCacheOperation();
        Integer temp = igniteCacheResource.getTimeToLive();
        if ("Add".equalsIgnoreCase(operation)) {
            if (temp != null) {
                timeToLive = temp;
            }
            createIgniteCluster.putData(key, value, cacheName, timeToLive);
        }
        if ("Get".equalsIgnoreCase(operation)) {
            cacheResource.setCacheValue(createIgniteCluster.getData(key, cacheName));
        }
        if ("GetAll".equalsIgnoreCase(operation)) {

            cacheResource.setCacheValue(String.valueOf(createIgniteCluster.getAllData(cacheName)));
        }

        cacheResource.setCacheKey(igniteCacheResource.getCacheKey());
        cacheResource.setCacheName(igniteCacheResource.getCacheName());
        cacheResource.setTimeToLive(timeToLive);
        cacheResource.setCacheOperation(igniteCacheResource.getCacheOperation());
        return cacheResource;

    }
}
