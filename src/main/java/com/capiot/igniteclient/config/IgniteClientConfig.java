package com.capiot.igniteclient.config;

import com.capiot.igniteclient.policies.IgniteRoutePolicy;
import org.apache.camel.CamelContext;
import org.apache.camel.spring.boot.CamelContextConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class IgniteClientConfig {

    private static final Logger log = LoggerFactory.getLogger(IgniteClientConfig.class);

//    public CacheConfiguration<Integer, String> getCacheConfiguration() {
//        CacheConfiguration<Integer, String> cacheConfiguration = new CacheConfiguration<Integer, String>();
//        cacheConfiguration.setEagerTtl(true);
//        return cacheConfiguration;
//    }

//    public CreateIgniteCluster getCreateIgniteCluster() {
//        CreateIgniteCluster createIgniteCluster = new CreateIgniteCluster();
//        createIgniteCluster.setIplist("iplist");
//        return createIgniteCluster;
//    }


//    private CreateIgniteCluster createIgniteCluster = getCreateIgniteCluster();
//    private CacheConfiguration cacheConfiguration = getCacheConfiguration();

    private IgniteRoutePolicy igniteRoutePolicy = new IgniteRoutePolicy();

    @Bean
    CamelContextConfiguration camelContextConfiguration() {

        return new CamelContextConfiguration() {
            @Override
            public void beforeApplicationStart(CamelContext camelContext) {
                camelContext.setMessageHistory(true);
                camelContext.setUseMDCLogging(true);
//                camelContext.getRegistry().bind("createIgniteCluster", createIgniteCluster);
//                camelContext.getRegistry().bind("cacheConfiguration", cacheConfiguration);
//                camelContext.getRegistry().bind("igniteRoutePolicy", igniteRoutePolicy);
            }

            @Override
            public void afterApplicationStart(CamelContext camelContext) {

            }
        };
    }

}
