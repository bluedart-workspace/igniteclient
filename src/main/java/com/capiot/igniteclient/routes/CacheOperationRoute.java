package com.capiot.igniteclient.routes;

import com.capiot.igniteclient.policies.IgniteRoutePolicy;
import com.capiot.igniteclient.processors.AccessCache;
import com.capiot.igniteclient.processors.CreateIgniteCluster;
import org.apache.camel.builder.RouteBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Component
public class CacheOperationRoute extends RouteBuilder {

    private static final Logger logger = LoggerFactory.getLogger(CacheOperationRoute.class);

    @Value("${ipList}")
    private String iplist;

    public CacheOperationRoute() {
    }

    private IgniteRoutePolicy igniteRoutePolicy = new IgniteRoutePolicy();

    @Autowired
    private AccessCache accessCache;


    @Override
    public void configure() throws Exception {

        from("timer://runOnce?repeatCount=1&delay=1000")
                .routeId("ClusterCreation")
                .routePolicy(igniteRoutePolicy)
                .bean(CreateIgniteCluster.class, "createCluster")
                .bean(CreateIgniteCluster.class, "toString")
                .log("IgniteIpList: ${body}")
        ;


        from("direct:AccessIgniteCacheGet")
                .routeId("AccessIgniteCacheGet")
                .setHeader("CacheOperation", simple("Get"))
                .threads()
                .poolSize(20)
                .maxPoolSize(50)
                .maxQueueSize(250)
                .bean(AccessCache.class, "process")
                .end()
                .log("Cache Get : ${headers}")
        ;


    }
}
