package com.capiot.igniteclient.controllers;

import com.capiot.igniteclient.processors.AccessCache;
import com.capiot.igniteclient.resources.IgniteCacheResource;
import org.apache.camel.ProducerTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IgniteClientController {

    private static final Logger log = LoggerFactory.getLogger(IgniteClientController.class);

    @Autowired
    private ProducerTemplate producerTemplate;

    @Autowired
    private AccessCache accessCache;

    @GetMapping("/getStatus")
    public ResponseEntity<String> getStatus() {
        log.info("Request received");
        return new ResponseEntity("Server is Running", HttpStatus.OK);
    }

    @PostMapping("/getCache")
    public ResponseEntity<String> getCache(@RequestBody IgniteCacheResource igniteCacheResource) {
        IgniteCacheResource igniteResource = new IgniteCacheResource();
        try {
            igniteResource = accessCache.process(igniteCacheResource);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<String>(igniteResource.getCacheValue(), HttpStatus.OK);
    }

    @PostMapping("/addCache")
    public ResponseEntity<String> addCache(@RequestBody IgniteCacheResource igniteCacheResource) {
        try {
            accessCache.process(igniteCacheResource);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<String>("Cache Added", HttpStatus.OK);

    }


}
